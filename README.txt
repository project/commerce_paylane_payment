CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

-- INTRODUCTION --

D8 Version of commerce paylane payment.

The Commerce Paylane Payment module provide a payment method for Drupal commerce
to Interact with Paylane payment gateway.This module redirect the user to 
Paylane paymeny gateway.ThIs module supports only two transaction 
types(Sale and authorization).


-- REQUIREMENTS --

Drupal Commerce(https://www.drupal.org/project/commerce)


-- INSTALLATION --

* Install as usual.


CONFIGURATION
-------------

 1. Enable the module under /admin/modules within the group 'Commerce(contrib)'.

 2. Goto admin/commerce/config/payment-gateways and add Payment gateway 
    as Paylane.Add the Key,Salt and Transacion Type.
	 

MAINTAINERS
-----------

Current maintainers:
- Ramesh - https://www.drupal.org/u/drupalramesh
