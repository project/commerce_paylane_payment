<?php

namespace Drupal\commerce_paylane_payment\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides the Paylane Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "paylane_offsite_redirect",
 *   label = "Paylane(Offsite Redirect)",
 *   display_label = "Paylane Credit Card",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_paylane_payment\PluginForm\OffsiteRedirect\PaylaneOffsiteForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "maestro", "mastercard", "visa",
 *   },
 * )
 */
class OffsiteRedirect extends OffsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merchant_key' => '',
      'salt' => '',
      'transaction_type' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $merchant_key = $this->configuration['merchant_key'];
    $salt = $this->configuration['salt'];
    $transaction_types = [
      'A' => 'Authorization',
      'S' => 'Sale',
    ];
    $default_transaction = $this->configuration['transaction_type'];

    $form['merchant_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Paylane Merchant Key'),
      '#default_value' => $merchant_key,
      '#required' => TRUE,
    ];
    $form['salt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Paylane account Salt'),
      '#default_value' => $salt,
      '#required' => TRUE,
    ];
    $form['transaction_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Transaction type'),
      '#options' => $transaction_types,
      '#default_value' => $default_transaction,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchant_key'] = $values['merchant_key'];
      $this->configuration['salt'] = $values['salt'];
      $this->configuration['transaction_type'] = $values['transaction_type'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $description = $request->get('description');
    $status = $request->get('status');
    $amount = $request->get('amount');
    $currency = $request->get('currency');
    $hash = $request->get('hash');
    $transaction_type = \Drupal::config("commerce_payment.commerce_payment_gateway." . $this->entityId)->getRawData()['configuration']['transaction_type'];
    if ($status !== 'ERROR' && $transaction_type == 'A') {
      $id = $request->get('id_authorization');
      $order_status = "authorization";
    }
    elseif ($status !== 'ERROR' && $transaction_type == 'S') {
      $id = $request->get('id_sale');
      $order_status = "completed";
    }
    $salt = \Drupal::config("commerce_payment.commerce_payment_gateway." . $this->entityId)->getRawData()['configuration']['salt'];
    $calc_hash = sha1("{$salt}|{$status}|{$description}|{$amount}|{$currency}|{$id}");
    if ($hash == $calc_hash && $status == 'PERFORMED') {
      $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
      $payment = $payment_storage->create([
        'state' => $order_status,
        'amount' => $order->getBalance(),
        'payment_gateway' => $this->parentEntity->id(),
        'order_id' => $order->id(),
        'remote_id' => $id,
        'remote_state' => $request->query->get('payment_status'),
      ]);
      $this->messenger()->addStatus($this->t('Your payment is successful'));
      $payment->save();
    }
    elseif ($hash == $calc_hash && $status == 'PENDING') {
      $this->messenger()->addError($this->t('Your payment is pending'));
    }
    else {
      $this->messenger()->addError($this->t('Your payment is Failed'));
    }
  }

}
