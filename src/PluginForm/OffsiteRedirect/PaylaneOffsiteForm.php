<?php

namespace Drupal\commerce_paylane_payment\PluginForm\OffsiteRedirect;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\commerce_order\Entity\Order;

/**
 * Provides the Off-site payment form.
 */
class PaylaneOffsiteForm extends PaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $order_id = \Drupal::routeMatch()->getParameter('commerce_order')->id();
    $order = Order::load($order_id);
    $key = $payment_gateway_plugin->getConfiguration()['merchant_key'];
    $salt = $payment_gateway_plugin->getConfiguration()['salt'];
    $amount = round($payment->getAmount()->getNumber(), 2);
    $transaction_description = '#' . $payment->getOrderId() . ': ' . $payment->getOrder()->getBillingProfile()->get('address')->first()->get('given_name')->getValue() . ': ' . $payment->getOrder()->getBillingProfile()->get('address')->first()->get('given_name')->getValue() . ' ' . $payment->getOrder()->getBillingProfile()->get('address')->first()->get('family_name')->getValue() . ':  ' . $payment->getOrder()->getStore()->getName();
    $description = \Drupal::routeMatch()->getParameter('commerce_order')->id();
    $transaction_type = $payment_gateway_plugin->getConfiguration()['transaction_type'];
    $cur = $order->getTotalPrice()->getCurrencyCode();
    $back_url = Url::fromUri('base:/checkout/' . $order_id . '/payment/return/', ['absolute' => TRUE])
      ->toString();
    $hash = sha1("{$salt}|{$description}|{$amount}|{$cur}|{$transaction_type}");
    $postData = [
      'amount' => $amount,
      'currency' => $cur,
      'merchant_id' => $key,
      'description' => $description,
      'transaction_description' => $transaction_description,
      'transaction_type' => $transaction_type,
      'back_url' => $back_url,
      'hash' => $hash,
    ];
    $url = "https://secure.paylane.com/order/cart.html";
    return $this->buildRedirectForm($form, $form_state, $url, $postData, PaymentOffsiteForm::REDIRECT_POST);
  }

}
